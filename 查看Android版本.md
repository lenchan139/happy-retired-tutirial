首先打開程式一覽，找到「設定」並輕點進入：    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/av-1.jpeg)    
之後移動到設定的最底部，會看見有「系統」或類似字眼的選項，點擊進入。    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/av-2.jpeg)    
之後再選擇「關於手機」    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/av-3.jpeg)    
之後就會看見「Android 系統」的項目，顯示你的Android版本。    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/av-4.jpeg)    