首先找到樂活新中年App的圖示，並長按約半秒放開，會彈出j選單，請按下「應用程式資訊」的選項。    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/hrv-1.jpeg)    
之後會看見類似下圖的界面，請滑動到最下方。    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/hrv-2.jpeg)    
會看見類似「版本 3.2.1」的字眼，3.2.1可以是其他數字組合。該組數字就是樂活App的版本。    
![](https://gitlab.com/lenchan139/happy-retired-tutirial/raw/master/img/hrv-3.jpeg)    
